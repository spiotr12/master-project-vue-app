import axios from 'axios';
import * as environment from '../config/config';

export class ValueService {

    static getValues(numberOfRecords, minChance, maxChance) {
        if (!minChance) {
            minChance = Math.round(numberOfRecords * (environment.keywordMinChance / 100))
        }
        if (!maxChance) {
            maxChance = Math.round(numberOfRecords * (environment.keywordMaxChance / 100))
        }

        let url = `${environment.apiUrl}/values/${numberOfRecords}?min=${minChance}&max=${maxChance}`;

        return axios.get(url);
    }
}
